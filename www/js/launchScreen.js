/**
 * Created by Robert on 19/06/2015.
 */

var launchScreen = document.getElementById('launch-screen');
launchScreen.addEventListener('touchstart', scanId, false);

function scanId() {
    cordova.plugins.barcodeScanner.scan(
        function(result) {
            if (result.text) {
                launchScreen.style.display = 'none';
                document.getElementById('menu-screen').style.display = 'block';
                previousScreens.push(currentScreen);
                currentScreen = 'menu-screen';
            }
        },
        function(error) {
            alert("Scanning failed: " + error);
        });
}
