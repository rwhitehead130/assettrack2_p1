var currentScreen = 'launch-screen';
var previousScreens = [];

var allItems = [{id: 'item1', name:'Soft Kinetic DS325', image:'img/softkineticds325.png', gridImage:'img/grid/softkineticds325.png', category:'Cameras', status:'Unavailable', currentHolder:'Me', previousHolder:'Hardware Lab', condition:'Good', problems:'RGB camera has problems within the Soft Kinetic SDK. Color image is shown to be faulty with obvious defect pixels.'},
    {id: 'item2', name:'Oculus Rift DK1', image:'img/oculusriftdk1.png', gridImage:'img/grid/oculusriftdk1.png', status:'Unavailable', category:'Head-mounted Wearables', currentHolder:'Me', previousHolder:'Hardware Lab', condition:'Good', problems:'None'},
    {id: 'item3', name:'Microsoft Kinect', image:'img/kinect.png', gridImage:'img/grid/kinect.png', status:'Unavailable', category:'Cameras', currentHolder:'Me', previousHolder:'Hardware Lab', condition:'Good', problems:'None'},
    {id: 'item4', name:'Oculus Rift DK2', image:'img/oculusriftdk2.png', gridImage:'img/grid/oculusriftdk2.png', status:'Unavailable,is taken away for usage in a conference.', category:'Head-mounted Wearables', currentHolder:'None', previousHolder:'Hardware Lab', condition:'Good', problems:'None'},
    {id: 'item5', name:'Arduino Kit', image:'img/arduinokit.png', gridImage:'img/grid/arduinokit.png', status:'Available', category:'Hardware', currentHolder:'Hardware Lab', previousHolder:'None', condition:'Fine', problems:'Missing quite a few components like LEDs, switches and wires. There is no manual with it too.'},
    {id: 'item6', name:'Aldebaran NAO robot', image:'img/naorobot.png', gridImage:'img/grid/naorobot.png', status:'Available', category:'Robot', currentHolder:'Hardware Lab', previousHolder:'Joshua', condition:'Good', problems:'None'},
    {id: 'item7', name:'Bostik glue gun', image:'img/gluegun.png', gridImage:'img/grid/gluegun.png', status:'Available', category:'Tools', currentHolder:'Hardware Lab', previousHolder:'None', condition:'Good', problems:'None'},
    {id: 'item8', name:'Dick Smith Multimeter', image:'img/multimeter.png', gridImage:'img/grid/multimeter.png', status:'Unavailable,Sent away for professional calibration to be done.', category:'Tools', currentHolder:'Hardware Lab', previousHolder:'None', condition:'Bad', problems:'Accuracy has gone off. It is no longer accurate enough to be used.'},
    {id: 'item9', name:'Electronic Digital Caliper', image:'img/digitalcaliper.png', gridImage:'img/grid/digitalcaliper.png', status:'Available', category:'Tools', currentHolder:'Hardware Lab', previousHolder:'None', condition:'Good', problems:'None'},
    {id: 'item10', name:'Black&Decker heat gun', image:'img/heatgun.png', gridImage:'img/grid/heatgun.png', status:'Available', category:'Tools', currentHolder:'Hardware Lab', previousHolder:'None', condition:'Good', problems:'None'},
    {id: 'item11', name:'Google glass', image:'img/googleglass.png', gridImage:'img/grid/googleglass.png', status:'Unavailable', category:'Head-mounted Wearables', currentHolder:'Robert', previousHolder:'Joshua', condition:'Good', problems:'Just be aware that the bone conduction transducer seems much weaker than usual. Therefore the volume might be soft.'}];

var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener('backbutton', this.backButton, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    backButton: function() {
        var lastScreen = previousScreens.pop();
        if (lastScreen !== undefined) {
            document.getElementById(currentScreen).style.display = 'none';
            document.getElementById(lastScreen).style.display = 'block';
            currentScreen = lastScreen;
        }
    }
};
