/**
 * Created by Robert on 19/06/2015.
 */

var itemContainer = document.getElementById('item-container');
drawBrowse(allItems);

function drawBrowse(itemArray) {
    itemContainer.innerHTML = '';
    for (var i = 0; i < allItems.length; i++) {
        (function(){
            var item = itemArray[i];
            var itemDiv = document.createElement('div');
            itemDiv.id = item.id;
            itemDiv.className = 'item';
            itemDiv.addEventListener('click', function(){showInfo(item)}, false);
            var itemDivImage = document.createElement('img');
            itemDivImage.className = 'item-image';
            itemDivImage.src = item.image;
            var itemDivName = document.createElement('div');
            itemDivName.className = 'item-text';
            itemDivName.innerHTML = item.name;
            var itemDivHolder = document.createElement('div');
            itemDivHolder.className = 'item-text';
            itemDivHolder.innerHTML = item.currentHolder;
            itemDiv.appendChild(itemDivImage);
            itemDiv.appendChild(itemDivHolder);
            itemDiv.appendChild(itemDivName);
            itemContainer.appendChild(itemDiv);
        })();
    }
}

function showInfo(item) {
    document.getElementById('browse-screen').style.display = 'none';
    document.getElementById('info-screen').style.display = 'block';
    previousScreens.push(currentScreen);
    currentScreen = 'info-screen';
    var infoText = document.getElementById('info-text');
    infoText.innerHTML = 'Name: ' + item.name + '<br>'
        + 'Current holder: ' + item.currentHolder + '<br>'
        + 'Previous holder: ' + item.previousHolder + '<br>'
        + 'Number in stock: ' + item.stock + '<br>'
        + 'Condition: ' + item.condition + '<br>'
        + 'Problems: ' + item.problems;
    var infoImage = document.getElementById('info-image');
    infoImage.src = item.image;
}

function searchItems() {
    document.getElementById('item-container').innerHTML = '';
    var searchTerm = document.getElementById('search-input').value.toLowerCase();
    if (searchTerm) {
        var searchResults = [];
        for (var i = 0; i < allItems.length; i++) {
            var item = allItems[i];
            if (item.name.toLowerCase().search(searchTerm) != -1) {
                searchResults.push(item)
            }
        }
        drawBrowse(searchResults);
    } else {
        drawBrowse(allItems);
    }
}
