/**
 * Created by Robert on 19/06/2015.
 */

var borrowButton = document.getElementById('borrow-button');
borrowButton.addEventListener('touchstart', borrowItem, false);

var returnButton = document.getElementById('return-button');
returnButton.addEventListener('touchstart', returnItem, false);

var browseButton = document.getElementById('browse-button');
browseButton.addEventListener('click', browseItems, false);

function borrowItem() {
    cordova.plugins.barcodeScanner.scan(
        function(result) {
            var qrText = result.text;
            if (qrText) {
                var prevHolder = null;
                for (var i = 0; i < allItems.length; i++) {
                    var item = allItems[i];
                    if (item.name == qrText) {
                        prevHolder = item.currentHolder;
                        if (prevHolder != 'Me') {
                            item.currentHolder = 'Me';
                            item.previousHolder = prevHolder;
                            drawBrowse(allItems);
                        }
                        break;
                    }
                }
                if (prevHolder != 'Me') {
                    alert(qrText + ' borrowed from ' + prevHolder);
                } else {
                    alert('You have already borrowed ' + qrText);
                }
            }
        },
        function(error) {
            alert("Scanning failed: " + error);
        });
}

function returnItem() {
    cordova.plugins.barcodeScanner.scan(
        function(result) {
            var qrText = result.text;
            if (qrText) {
                var currentHolder = null;
                var prevHolder = null;
                for (var i = 0; i < allItems.length; i++) {
                    var item = allItems[i];
                    if (item.name == qrText) {
                        currentHolder = item.currentHolder;
                        prevHolder = item.previousHolder;
                        if (currentHolder == 'Me') {
                            item.currentHolder = item.previousHolder;
                            item.previousHolder = 'None';
                            drawBrowse(allItems);
                        }
                        break;
                    }
                }
                if (currentHolder == 'Me') {
                    alert(qrText + ' returned to ' + prevHolder);
                } else {
                    alert('You are not the current borrower of ' + qrText);
                }
            }
        },
        function(error) {
            alert("Scanning failed: " + error);
        });
}

function browseItems() {
    document.getElementById('menu-screen').style.display = 'none';
    document.getElementById('browse-screen').style.display = 'block';
    previousScreens.push(currentScreen);
    currentScreen = 'browse-screen';
}